# Rockup Script - README #
---

### Overview ###

The **Rockup** script (Batch) will mirror all the data from the specified source directory to the specified destination directory using the **Robocopy** tool from **Microsoft**. Please download the **Windows Server 2003 Resource Kit Tools** which contain the **Robocopy Executable (Version XP010)** from the following website:

Download: [**Windows Server 2003 Resource Kit Tools**](https://www.microsoft.com/en-us/download/details.aspx?displaylang=en&id=17657)

### Screenshots ###

![Rockup - Drive Info](development/readme/rockup1.png "Rockup - Drive Info")

![Rockup - Check Drives](development/readme/rockup2.png "Rockup - Check Drives")

![Rockup - Backup](development/readme/rockup3.png "Rockup - Backup")

![Rockup - Finished](development/readme/rockup4.png "Rockup - Finished")


### Setup ###

* Copy the directory **rockup** to your computer.
* Download the [**Windows Server 2003 Resource Kit Tools**](https://www.microsoft.com/en-us/download/details.aspx?displaylang=en&id=17657).
* Copy the **Robocopy Executable** to the directory **rockup**.
* Edit the configuration section of the **Rockup.bat** script to specify the source and destination folder.
* Save the **Rockup** script.
* Connect the backup drives (internal or external harddisks, flashdrives, etc...) to your PC.
* Doubleclick the script file **Rockup.bat** to start the backup.
* **WARNING: Please check that the source and destination directories are specified correctly before starting the backup !!!!**

### Support ###

This is a free tool and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **Rockup** script is licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](https://opensource.org/licenses/MIT).
