@ECHO OFF

REM ####################################################################
REM #                                                                  #
REM #                                                                  #
REM #         R O C K U P   -   R O B O C O P Y   B A C K U P          #
REM #                                                                  #
REM #            Copyright 2015 by PB-Soft / Patrick Biegel            #
REM #                                                                  #
REM #            Script version 2.0 / Robocopy version XP010           #
REM #                                                                  #
REM #                         www.pb-soft.com                          #
REM #                                                                  #
REM #                                                                  #
REM ####################################################################
REM #                                                                  #
REM #    Open, edit and save this file with the following encoding:    #
REM #                                                                  #
REM #                          DOS (CP 437)                            #
REM #                                                                  #
REM ####################################################################

REM ====================================================================
REM   C O N F I G U R A T I O N   S E C T I O N   -   B E G I N
REM ====================================================================

REM Set the background and text color.
COLOR 18

REM Set the backup name.
SET BACKUP_NAME=FULL BACKUP

REM Set the data source.
SET DATA_SOURCE=C:\

REM Set the data destination.
SET DATA_DESTINATION=X:\

REM Set the logfile name.
SET LOGFILE_NAME=Backup.log

REM Set the Robocopy options.
REM
REM  /L    -  Run only a test without copying files.
REM  /NDL  -  Don't log directories.
REM  /NFL  -  Don't log files.
REM
SET ROBOCOPY_OPTIONS=/R:10 /W:30 /ZB /COPY:DAT /MIR /TEE /LOG:%LOGFILE_NAME%

REM ====================================================================
REM   C O N F I G U R A T I O N   S E C T I O N   -   E N D
REM ====================================================================

CLS
ECHO ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ
ECHO ³ R O C K U P - %BACKUP_NAME% - Display Drive Information
ECHO ÃÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ
ECHO ³
ECHO ³  Using the following two drive letters:
ECHO ³
ECHO ³
ECHO ³  - Data source:       %DATA_SOURCE%
ECHO ³
ECHO ³  - Data destination:  %DATA_DESTINATION%
ECHO ³
ECHO ³
ECHO ³  The data will be mirrored from %DATA_SOURCE% to %DATA_DESTINATION% !
ECHO ³
ECHO ³
ECHO ÃÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ
ECHO ³
ECHO ³  Now connect your drives and then:
ECHO ³
ECHO ³  - Press any key to continue the backup
ECHO ³
ECHO ³  - Press CTRL + C to abort the backup
ECHO ³
ECHO ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ
PAUSE > nul

CLS
ECHO ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ
ECHO ³ R O C K U P - %BACKUP_NAME% - Check the Drives and the Robocopy Application
ECHO ÃÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ
ECHO ³
IF NOT EXIST %DATA_SOURCE% GOTO SOURCE_ERROR
ECHO ³  OK - Data source %DATA_SOURCE% was found.
ECHO ³
IF NOT EXIST %DATA_DESTINATION% GOTO DESTINATION_ERROR
ECHO ³  OK - Data destination %DATA_DESTINATION% was found.
ECHO ³
IF NOT EXIST Robocopy.exe GOTO ROBOCOPY_ERROR
ECHO ³  OK - The Robocopy tool was found.
ECHO ³
ECHO ³  The following command will mirror the data from %DATA_SOURCE% to %DATA_DESTINATION% !
ECHO ³
ECHO ³  ROBOCOPY %DATA_SOURCE% %DATA_DESTINATION% %ROBOCOPY_OPTIONS%
ECHO ³
ECHO ³
ECHO ÃÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ
ECHO ³
ECHO ³  Now check that your drive letters are correct and then:
ECHO ³
ECHO ³  - Press any key to continue the backup
ECHO ³
ECHO ³  - Press CTRL + C to abort the backup
ECHO ³
ECHO ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ
PAUSE > nul

CLS
ECHO ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ
ECHO ³ R O C K U P - %BACKUP_NAME% - Copy Data
ECHO ÃÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ
ECHO ³
ECHO ³  Copy the data from %DATA_SOURCE% to %DATA_DESTINATION% ...
ECHO ³
ECHO ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ B A C K U P   S T A R T ÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ
ECHO.
Robocopy.exe %DATA_SOURCE% %DATA_DESTINATION% %ROBOCOPY_OPTIONS%
ECHO.
ECHO ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ B A C K U P   E N D ÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ
ECHO ³
ECHO ³  The backup process has terminated !
ECHO ³
ECHO ÃÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ
ECHO ³
ECHO ³  - Press any key to continue and display the logfile
ECHO ³
ECHO ³  - Press CTRL + C to exit the backup script
ECHO ³
ECHO ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ
PAUSE > nul

CLS
ECHO ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ
ECHO ³ R O C K U P - %BACKUP_NAME% - Display the Logfile
ECHO ÃÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ
ECHO ³
IF NOT EXIST %LOGFILE_NAME% GOTO LOGFILE_ERROR
ECHO ³  Open the log file %LOGFILE_NAME% ...
ECHO ³
ECHO ÃÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ
ECHO ³
ECHO ³  - Press any key to exit the backup script
ECHO ³
ECHO ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ
NOTEPAD.EXE %LOGFILE_NAME%
PAUSE > nul

GOTO END2

:SOURCE_ERROR
ECHO ³  Could not find the data source %DATA_SOURCE% !
GOTO END1

:DESTINATION_ERROR
ECHO ³  Could not find the data destination %DATA_DESTINATION% !
GOTO END1

:ROBOCOPY_ERROR
ECHO ³  Could not find the copy tool 'Robocopy.exe' !
GOTO END1

:LOGFILE_ERROR
ECHO ³  Could not find the logfile '%LOGFILE_NAME%' !
GOTO END1

:END1
ECHO ³
ECHO ÃÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ
ECHO ³
ECHO ³  - Press any key to exit the backup script
ECHO ³
ECHO ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ
PAUSE > nul

:END2
