
Important:

Please copy the file "Robocopy.exe" to this directory !
This tool is needed by the backup script to work!

The Robocopy tool is part of the "Microsoft Windows Server 2003 Resource Kit Tools" which you can download from the following Microsoft website:

Download: http://www.microsoft.com/en-us/download/details.aspx?displaylang=en&id=17657

Please also check the Robocopy version. You need the version from the "Microsoft Windows Server 2003 Resource Kit Tools" which is the version "XP010". To check the actual version of your robocopy application please start the application from the command line with the parameter /? to show the help text:

  Robocopy.exe /?

The output will show you the version in the title: ----------------------|
                                                                         |
                                                                         v
-------------------------------------------------------------------------------
   ROBOCOPY     ::     Robust File Copy for Windows     ::     Version XP010
-------------------------------------------------------------------------------


If the comand from above does not show the help text with the version information, your version is not the correct one!
